module Arcade.Random.MT
    ( State
    , newState
    , getVec
    , getIdx
    , next
    , temper
    , twist
    , randomSeq
    ) where

import Control.Monad (forM_)
import Control.Monad.ST (ST, runST)
import Data.Bits ((.&.), (.|.), complement, shiftL, shiftR, xor)
import Data.Vector.Unboxed ((!))
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as VM
import Data.Word (Word32)


data State = State
    { vec :: !(V.Vector Word32)
    , idx :: !Int
    } deriving (Eq, Show)


getVec :: State -> V.Vector Word32
getVec State{vec} = vec


getIdx :: State -> Int
getIdx State{idx} = idx


-- Coefficients
mtW =  32 :: Int
mtN = 624 :: Int
mtM = 397 :: Int
mtR =  31 :: Int
mtU =  11 :: Int
mtS =   7 :: Int
mtT =  15 :: Int
mtL =  18 :: Int
mtD = 0xffffffff :: Word32  -- already implicit with modular Word32 arithmetic
mtB = 0x9d2c5680 :: Word32
mtC = 0xefc60000 :: Word32
mtA = 0x9908b0df :: Word32
mtF = 1812433253 :: Word32

-- 0x7fffffff
lower_mask :: Word32
lower_mask = (1 `shiftL` mtR) - 1

-- 0x80000000
upper_mask :: Word32
upper_mask = complement lower_mask


newState :: Word32 -> State
newState seed = State initialVec initialIdx
  where
    initialVec = (V.fromList (scanl step seed [1..mtN-1]))
    initialIdx = mtN

    step :: Word32 -> Int -> Word32
    step prev i = mtF * (prev `xor` (prev `shiftR` (mtW - 2))) + fromIntegral i


next :: State -> (Word32, State)
next State{vec,idx}
    | idx < mtN = (temper (vec ! idx), State vec (idx + 1))
    | otherwise = (temper (V.head newVec), State newVec (newIdx + 1))

  where
    State newVec newIdx = twist $ State vec idx


temper :: Word32 -> Word32
temper = y4 . y3 . y2 . y1
y1 = \y -> y `xor` ((y `shiftR` mtU) .&. mtD)
y2 = \y -> y `xor` ((y `shiftL` mtS) .&. mtB)
y3 = \y -> y `xor` ((y `shiftL` mtT) .&. mtC)
y4 = \y -> y `xor` (y `shiftR` mtL)


twist :: State -> State
twist (State vec idx) =
  let
    middle = vec ! ((idx + mtM) `mod` mtN)

    mixbits :: Word32 -> Word32 -> Word32
    mixbits u v = (u .&. upper_mask) .|. (v .&. lower_mask)

    twistOne :: Word32 -> Word32 -> Word32
    twistOne u v = (mixbits u v `shiftR` 1) `xor` n `xor` middle
        where n
                | v .&. 1 == 1 = mtA  -- odd - mix with array A
                | otherwise    = 0    -- even - do nothing

    twistAt :: V.MVector s Word32 -> Int -> ST s (V.MVector s Word32)
    twistAt mutVec i = do
        u <- VM.read mutVec i
        v <- VM.read mutVec ((i + 1) `mod` mtN)
        VM.write mutVec i $ twistOne u v
        pure $ mutVec

    twistAll :: V.Vector Word32 -> V.Vector Word32
    twistAll vec = runST $ do
        mutVec <- V.thaw vec
        forM_ [0..mtN-1] (\i -> twistAt mutVec i)
        V.freeze mutVec

  in
    State (twistAll vec) 0


firstWord :: State -> Word32
firstWord State{vec} = V.head vec


lastWord :: State -> Word32
lastWord State{vec} = V.last vec


randomSeq :: Word32 -> Int -> ([Word32], State)
randomSeq seed 0 = ([], newState seed)
randomSeq seed n = (ws <> [w], sn)
  where
    (w, sn) = next s
    (ws, s) = randomSeq seed (n-1)
