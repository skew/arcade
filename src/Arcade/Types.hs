module Arcade.Types where

import RIO (ByteString)
import qualified RIO.Vector.Unboxed as V


data TokenStats = TokenStats
    { total      :: !Integer
    , minLength  :: !Int
    , maxLength  :: !Int
    , lengthFreq :: !(V.Vector Int)
    , byteFreq   :: !(V.Vector Int)
    } deriving (Eq, Show)


isAscii :: TokenStats -> Bool
isAscii TokenStats{byteFreq} =
    V.slice 128 128 byteFreq == V.replicate 128 0


isBase64 :: TokenStats -> Bool
isBase64 stats = and
    [ isAscii stats
    , V.slice 0 43 (byteFreq stats) == V.replicate 43 0
    , V.slice 44 3 (byteFreq stats) == V.replicate 3 0
    , V.slice 58 3 (byteFreq stats) == V.replicate 3 0
    , V.slice 62 3 (byteFreq stats) == V.replicate 3 0
    , V.slice 91 6 (byteFreq stats) == V.replicate 6 0
    , V.slice 123 5 (byteFreq stats) == V.replicate 5 0
    , minLength stats `mod` 4 == 0
    , maxLength stats `mod` 4 == 0
    ]


isAlphaNum :: TokenStats -> Bool
isAlphaNum stats = and
    [ isAscii stats
    , V.slice 0 48 (byteFreq stats) == V.replicate 48 0
    , V.slice 58 7 (byteFreq stats) == V.replicate 7 0
    , V.slice 91 6 (byteFreq stats) == V.replicate 6 0
    , V.slice 123 5 (byteFreq stats) == V.replicate 5 0
    ]


isHex :: TokenStats -> Bool
isHex stats = and
    [ isAlphaNum stats
    , V.slice 71 20 (byteFreq stats) == V.replicate 20 0
    , V.slice 103 20 (byteFreq stats) == V.replicate 20 0
    , minLength stats `mod` 2 == 0
    , maxLength stats `mod` 2 == 0
    ]


{-
data Token = Token
    { length :: Integer
    }


newtype TokenValue = TokenValue ByteString


data Result
    = ResUnknown
    | ResHash
    | ResCrypt
-}
