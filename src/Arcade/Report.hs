module Arcade.Report where

import Arcade.Types
import Arcade.Charts
import qualified RIO.Text as T


report :: TokenStats -> String
report stats = unlines
    [ ""
    , "Token Statistics:"
    , replicate 72 '='
    , "Total Tokens: " <> (show $ total stats)
    , "Min Length:   " <> (show $ minLength stats)
    , "Max Length:   " <> (show $ maxLength stats)
    , "Ascii:        " <> (show $ isAscii stats)
    , "Base64:       " <> (show $ isBase64 stats)
    , "Alphanumeric: " <> (show $ isAlphaNum stats)
    , "Hex (Base16): " <> (show $ isHex stats)
    , ""
    , "Token Lengths:"
    , replicate 72 '='
    , T.unpack $ histoFreq (lengthFreq stats) (fromIntegral $ total stats) labelInt
    , ""
    , "Byte Distribution:"
    , replicate 72 '='
    , T.unpack $ histoFreq
        (byteFreq stats) (fromIntegral $ total stats) labelHex
    ]
