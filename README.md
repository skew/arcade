# arcade

`arcade` is a tool for analyzing security token values. 

## Installation

Releases are coming. For now, you can build using the `stack` tool:

`stack build`
