{-# LANGUAGE NamedFieldPuns #-}

import Options.Applicative
import qualified Arcade.Common as Arcade


main :: IO ()
main = do
    Args{input} <- execParser opts
    case input of
        FileInput filepath -> Arcade.run filepath
        StdInput           -> Arcade.runStdIn
  where
    opts = info (args <**> helper)
        ( fullDesc
        <> header "test")


data Args = Args
    { verbose :: Bool
    , input   :: Input
    } deriving Show


data Input
    = FileInput FilePath
    | StdInput
    deriving Show


args :: Parser Args
args = Args
    <$> switch
        (long "verbose" <> short 'v' <> help "Verbose output")
    <*> (fileInput <|> stdInput)


fileInput :: Parser Input
fileInput = FileInput <$> strOption
    (  long "file"
    <> short 'f'
    <> metavar "FILENAME"
    <> help "Read from file"
    )


stdInput :: Parser Input
stdInput = flag' StdInput
    (  long "stdin"
    <> help "Read from stdin"
    )
