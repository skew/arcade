module Arcade.Charts where

import Numeric (showHex)
import RIO (Text)
import RIO.Char.Partial (chr)
import qualified RIO.Text as T
import RIO.Vector.Unboxed (Vector)
import qualified RIO.Vector.Unboxed as V
import qualified RIO.Vector.Unboxed.Partial as V (maximum)


histoFreq :: Vector Int -> Int -> (Int -> Text) -> Text
histoFreq freq maxValue label =
    let width = 64
        block = "\x2589" :: Text
        --barlen c = barLength width maxValue c
        barlen c = barLength width (V.maximum freq) c
        row i c  =
            if c == 0
                then ""
                else label i <> " " <> T.replicate (barlen c) block <> "\x258f\n"
        accum i c t = row i c <> t
    in  V.ifoldr' accum "" freq


labelHex :: Int -> Text
labelHex i =
    let h = T.pack $ showHex i ""
        c = T.singleton (chr i)
    in  "0x" <> T.replicate (2 - T.length h) "0" <> h <> " " <> c


labelInt :: Int -> Text
labelInt i =
    let pad = 4
    in  T.replicate (pad - length (show i)) " " <> T.pack (show i)


barLength :: Int -> Int -> Int -> Int
barLength maxLength maxValue thisValue =
    let (whole, parts) = quotRem (maxLength * thisValue) maxValue
    in  whole
