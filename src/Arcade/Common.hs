module Arcade.Common where

import Conduit
import Control.Monad.ST (ST, runST)
import Crypto.Hash
import Data.ByteArray (convert)
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import Data.Mutable
import Data.Serialize (encode)
import Data.STRef
import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as VM

import Arcade.Types
import Arcade.Report


run :: FilePath -> IO ()
run filepath = do
    stats <- runConduitRes
        $ sourceFile filepath
       .| linesUnboundedAsciiC
       .| sinkStats
    putStrLn $ report stats


runStdIn :: IO ()
runStdIn = do
    stats <- runConduitRes
        $ stdinC
       .| linesUnboundedAsciiC
       .| sinkStats
    putStrLn $ report stats


generate :: IO ()
generate =
    runConduit
        $ yieldMany [1..999]
       .| mapC (encode :: Integer -> ByteString)
       .| mapC (hashWith MD5)
       .| mapM_C print


genTokens :: ConduitT () ByteString IO ()
genTokens =
    yieldMany [1..999]
       .| mapC (encode :: Integer -> ByteString)
       .| mapC (hashWith MD5)
       .| mapC convert


sinkStats :: PrimMonad m => ConduitT ByteString Void m TokenStats
sinkStats = getZipSink $
    TokenStats
        <$> ZipSink sinkCount
        <*> ZipSink sinkMinLength
        <*> ZipSink sinkMaxLength
        <*> ZipSink sinkLengthFreq
        <*> ZipSink sinkByteFreq


sinkStatsIO :: ConduitT ByteString Void IO TokenStats
sinkStatsIO = undefined


sinkCount :: PrimMonad m => ConduitT a Void m Integer
sinkCount = do
    refCount <- asMutVar <$> newRef 0
    mapM_C $ \_ -> do
        modifyRef' refCount (+1)
    lift $ readRef refCount


sinkMinLength :: PrimMonad m => ConduitT ByteString Void m Int
sinkMinLength = do
    refMin <- asMutVar <$> newRef 9000
    mapM_C $ \t -> do
        modifyRef' refMin $ min (B.length t)
    lift $ readRef refMin


sinkMaxLength :: PrimMonad m => ConduitT ByteString Void m Int
sinkMaxLength = do
    refMax  <- asMutVar <$> newRef 0
    mapM_C $ \t -> do
        modifyRef' refMax $ max (B.length t)
    lift $ readRef refMax


sinkMinMax :: ConduitT ByteString Void (ST s) (Int, Int)
sinkMinMax = getZipSink $ (,) <$> ZipSink sinkMinLength <*> ZipSink sinkMaxLength


sinkLengthFreq :: PrimMonad m => ConduitT ByteString Void m (V.Vector Int)
sinkLengthFreq = do
    refFreq <- lift $ VM.replicate 256 0  -- TODO allow arbitrary lengths
    mapM_C $ \t -> do
        let index = B.length t
        VM.modify refFreq (+1) index
    lift $ V.freeze refFreq


sinkByteFreq :: PrimMonad m => ConduitT ByteString Void m (V.Vector Int)
sinkByteFreq = do
    refFreq <- lift $ VM.replicate 256 0
    mapM_CE $ \w -> do
        let index = fromIntegral w
        VM.modify refFreq (+1) index
    lift $ V.freeze refFreq


testCount :: ST s Integer
testCount = runConduit
            $ yieldMany [1..999]
           .| mapC (encode :: Integer -> ByteString)
           .| sinkCount


testCount' :: IO ()
testCount' = do
    let c = runST $ runConduit $ yieldMany [1..999] .| sinkCount
    print c


sourceFruit :: Monad m => ConduitT () ByteString m ()
sourceFruit = yieldMany ["apple", "banana", "cantaloupe"]


testStats :: TokenStats
testStats = runST $ runConduit $ sourceFruit .| sinkStats


data TokenHint
    = TokenHex
    | TokenBase64
    | TokenUnknown


classify :: TokenStats -> TokenHint
classify =
    undefined
